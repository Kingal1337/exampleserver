package exampleserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Alan T
 */
public class Client {
    private Socket socket;
    private DataInputStream input;
    private DataOutputStream output;
    public static void main(String[] args) throws IOException{
        Client client = new Client("localhost",7777);
    }
    public Client(String ip,int port) throws IOException{
        socket = new Socket(ip,port);
        input = new DataInputStream(socket.getInputStream());
        output = new DataOutputStream(socket.getOutputStream());
        MessageGetter messageGetter = new MessageGetter(input);
        Thread thread = new Thread(messageGetter);
        thread.start();
        Scanner scanner = new Scanner(System.in);
        while(true){
            if(scanner.hasNextLine()){
                sendMessage(scanner);
            }
        }
    }
    
    public void sendMessage(Scanner scanner) throws IOException{
        output.writeUTF(scanner.nextLine());        
    }
    
    class MessageGetter implements Runnable{ 
        private DataInputStream input;
        public MessageGetter(DataInputStream input){
            this.input = input;
        }

        @Override
        public void run() {
            while(true){
                String message;
                try{
                    message = input.readUTF();
                    System.out.println(message);
                } catch(Exception e){

                }
            }
        }
    }
}
