package exampleserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author Alan T
 */
public class Server {
    private ServerSocket serverSocket;
    private ArrayList<User> allClients;
    public static void main(String[] args) throws IOException{
        Server server = new Server(7777);
    }
    
    public Server(int port) throws IOException{
        allClients = new ArrayList<>();
        System.out.println("Starting the server");
        serverSocket = new ServerSocket(7777);
        System.out.println("Server has started");
        while(true){
            Socket clientSocket = serverSocket.accept();
            System.out.println("Client Connected From : "+clientSocket.getInetAddress());
            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
            DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
            User user = new User(clientSocket,input, output);
            allClients.add(user);
            Thread thread = new Thread(user);
            thread.start();
        }
    }
    
    class User implements Runnable{
        private Socket client;
        private DataOutputStream output;
        private DataInputStream input;
        
        public User(Socket client,DataInputStream input,DataOutputStream output){
            this.client = client;
            this.input = input;
            this.output = output;
        }
        
        @Override
        public void run() {
            while(true){
                try{
                    String message = input.readUTF();
                    for(User user : allClients){
                        if(user.getClient() != client){
                            user.getOutput().writeUTF(message);
                        }
                    }
                }catch(Exception e){
                    
                }
            }                
        }

        public Socket getClient() {
            return client;
        }

        public void setClient(Socket client) {
            this.client = client;
        }

        public DataOutputStream getOutput() {
            return output;
        }

        public void setOutput(DataOutputStream output) {
            this.output = output;
        }

        public DataInputStream getInput() {
            return input;
        }

        public void setInput(DataInputStream input) {
            this.input = input;
        }
        
        
    }
}
